const express = require("express");
const router = express.Router();
const {
  getRoutines,
  getRoutineId,
  createRoutine,
  editRoutineId,
  deleteRoutineId,
} = require("../controllers/routine");

router.get("/", getRoutines);
router.get("/:id", getRoutineId);
router.post("/", createRoutine);
router.put("/:id", editRoutineId);
router.delete("/:id", deleteRoutineId);

module.exports = router;
