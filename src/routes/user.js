const express = require("express");
const router = express.Router();
const {
  getUsers,
  getUserId,
  createUser,
  editUserId,
  deleteUserId,
  addRoutineUser,
} = require("../controllers/user");

router.put("/routine/:id", addRoutineUser);
router.get("/", getUsers);
router.get("/:id", getUserId);
router.post("/", createUser);
router.put("/:id", editUserId);
router.delete("/:id", deleteUserId);

module.exports = router;
