const express = require("express");
const { mongoose } = require("mongoose");
require("dotenv").config();

const routineRoutes = require("./routes/routine");
const userRoutes = require("./routes/user");

const app = express();
app.use(express.json());

// ENV variables
const { DB_HOST, DB_PORT, DB_NAME, PORT } = process.env;
const port = PORT || 3000;
const uri = `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`;

// Routes
app.get("/", (req, res) => res.send("Hello World!"));
app.use("/users", userRoutes);
app.use("/routines", routineRoutes);

// Mongoose configuration
mongoose.set("strictQuery", false);
mongoose
  .connect(uri)
  .then(() => console.log("Connected"))
  .catch((err) => console.log(err));

// Port
app.listen(port, () => console.log(`Example app listening on port ${port}`));
