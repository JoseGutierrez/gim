const mongoose = require("mongoose");
const Routine = mongoose.model("Routine");

const userSchema = mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  routine: [{ type: mongoose.Schema.Types.ObjectId, ref: "Routine" }],
});

module.exports = mongoose.model("User", userSchema);
