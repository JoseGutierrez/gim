const mongoose = require("mongoose");

const routineSchema = mongoose.Schema({
  name: { type: String, required: true },
  series: { type: Number, required: true },
  repetitions: { type: Number, required: true },
});

module.exports = mongoose.model("Routine", routineSchema);
