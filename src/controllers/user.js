const User = require("../models/user");
const bcrypt = require("bcrypt");

const getUsers = async (req, res) => {
  const users = await User.find();
  return res.status(200).send({ users });
};

const getUserId = async (req, res) => {
  try {
    const { id } = req.params;
    const users = await User.find();
    const user = users.find((item) => item.id == id);
    if (!user) return res.status(404).send({ message: "User Not Found" });
    return res.status(200).send({ user });
  } catch {
    return res.status(500).send({ message: "Find User Failed" });
  }
};

const createUser = async (req, res) => {
  try {
    const password = bcrypt.hashSync(req.body.password, 10);
    await User({ ...req.body, password }).save();
    return res.status(201).send({ message: "Create User Successfully" });
  } catch (err) {
    return res.status(500).send({ message: "Create User Failed" });
  }
};

const editUserId = async (req, res) => {
  try {
    const { id } = req.params;
    const password = bcrypt.hashSync(req.body.password, 10);
    await User.updateOne({ _id: id }, { $set: { ...req.body, password } });
    return res.status(201).send({ message: "Edit User Successfully" });
  } catch (err) {
    return res.status(500).send({ message: "Edit User Failed" });
  }
};

const deleteUserId = async (req, res) => {
  try {
    const { id } = req.params;
    await User.remove({ _id: id });
    return res.status(201).send({ message: "Delete User Successfully" });
  } catch (err) {
    return res.status(500).send({ message: "Delete User Failed" });
  }
};

const addRoutineUser = async (req, res) => {
  try {
    const { id } = req.params;
    const newRoutine = req.body.routine;
    const users = await User.find();
    const user = users.find((item) => item.id == id);
    if (!user) return res.status(404).send({ message: "User Not Found" });
    const routine = user.routine;
    routine.push(newRoutine);
    await User.updateOne({ _id: id }, { $set: { ...user, routine } });
    return res.status(201).send({ message: "Add Routine Successfully" });
  } catch (err) {
    return res.status(500).send({ message: "Add Routine Failed" });
  }
};

module.exports = {
  getUsers,
  getUserId,
  createUser,
  editUserId,
  deleteUserId,
  addRoutineUser,
};
