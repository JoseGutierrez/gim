const Routine = require("../models/routine");

const getRoutines = async (req, res) => {
  const routines = await Routine.find();
  return res.status(200).send({ routines });
};

const getRoutineId = async (req, res) => {
  try {
    const { id } = req.params;
    const routines = await Routine.find();
    const routine = routines.find((item) => item.id == id);
    if (!routine) return res.status(404).send({ message: "Routine Not Found" });
    return res.status(200).send({ routine });
  } catch {
    return res.status(500).send({ message: "Find Routine Failed" });
  }
};

const createRoutine = async (req, res) => {
  try {
    await Routine(req.body).save();
    return res.status(201).send({ message: "Create Routine Successfully" });
  } catch (err) {
    return res.status(500).send({ message: "Create Routine Failed" });
  }
};

const editRoutineId = async (req, res) => {
  try {
    const { id } = req.params;
    await Routine.updateOne({ _id: id }, { $set: req.body });
    return res.status(201).send({ message: "Edit Routine Successfully" });
  } catch (err) {
    return res.status(500).send({ message: "Edit Routine Failed" });
  }
};

const deleteRoutineId = async (req, res) => {
  try {
    const { id } = req.params;
    await Routine.remove({ _id: id });
    return res.status(201).send({ message: "Delete Routine Successfully" });
  } catch (err) {
    return res.status(500).send({ message: "Delete Routine Failed" });
  }
};

module.exports = {
  getRoutines,
  getRoutineId,
  createRoutine,
  editRoutineId,
  deleteRoutineId,
};
